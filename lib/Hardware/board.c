#include "board.h"
#include "ds18b20.h"
#include "driver/gpio.h"

static fnButton Bnfn = NULL;
uint8_t gpio_value = 0;

void GPIO_Init(fnButton fnB)
{
    if (Bnfn != NULL)
        Bnfn = NULL;
    gpio_config_t io_config = {};
    Bnfn = fnB;
    // Config Output
    io_config.intr_type = GPIO_INTR_DISABLE;
    io_config.mode = GPIO_MODE_OUTPUT;
    io_config.pin_bit_mask = GPIO_LED_PIN_SEL;
    io_config.pull_up_en = GPIO_PULLUP_DISABLE;
    io_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    gpio_config(&io_config);

    // Config Input
    io_config.intr_type = GPIO_INTR_POSEDGE;
    io_config.mode = GPIO_MODE_INPUT;
    io_config.pin_bit_mask = GPIO_BUTTON_PIN_SEL;
    io_config.pull_up_en = GPIO_PULLUP_ENABLE;
    gpio_config(&io_config);
    gpio_install_isr_service(0);
    gpio_set_level(LED_PINOUT, gpio_value);
    gpio_isr_handler_add(GPIO_NUM_0, Bnfn, (void *)GPIO_NUM_0);
}

uint8_t GPIO_Set(uint8_t value)
{
    if (value == gpio_value || value > 2)
        return gpio_value;
    gpio_set_level(LED_PINOUT, value);
    gpio_value = value;
    return gpio_value;
}

uint8_t GPIO_Toggle()
{
    return GPIO_Set(!gpio_value);
}


void TempSensor_Init(){
    ds18b20_init(TEMP_BUS);
    ds18b20_requestTemperatures();
}