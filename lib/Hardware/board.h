#pragma once

#include "define.h"
#define LED_PINOUT GPIO_NUM_2
#define BUTTON_PINOUT GPIO_NUM_0
#define TEMP_BUS 26
#define GPIO_LED_PIN_SEL (1ULL << LED_PINOUT)
#define GPIO_BUTTON_PIN_SEL (1ULL << BUTTON_PINOUT)
typedef void (*fnButton)();
void GPIO_Init(fnButton fnB);
uint8_t GPIO_Set(uint8_t value);
uint8_t GPIO_Toggle();
void TempSensor_Init();