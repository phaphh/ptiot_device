#pragma once
#include "define.h"
#include "esp_mesh.h"
void NetworkInit();

void list_cb_add(char *key, mqtt_mess_cb_t v);
void list_cb_print();
extern uint8_t STAMac[6];
extern uint8_t APMac[6];
extern uint8_t Root[6];
extern uint8_t PMAC[6];
extern mesh_addr_t mesh_parent_addr;
extern uint8_t isRoot;