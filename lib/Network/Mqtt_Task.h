#pragma once
#include "define.h"

void mqtt_app_start(void);
extern QueueHandle_t publishQueue;
extern QueueHandle_t receiveQueue;
extern bool isPubTaskRunning;
extern bool isSubTaskRunning;
extern uint8_t STAMac[6];