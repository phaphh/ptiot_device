#include "mqtt_client.h"
#include "define.h"
#include "esp_event.h"
#include "esp_log.h"
#include <stdio.h>
#include <stdlib.h>
#include "Mqtt_Task.h"
#include "esp_random.h"
QueueHandle_t receiveQueue;
bool isPubTaskRunning = false;
bool isSubTaskRunning = false;

static const char *TAG = "mesh_mqtt";
static esp_mqtt_client_handle_t s_client = NULL;

struct mqtt_node_t *list_cb = NULL;

void mqtt_app_publish(char *topic, char *publish_string)
{
    if (s_client)
    {
        int msg_id = esp_mqtt_client_publish(s_client, topic, publish_string, 0, 1, 0);
        ESP_LOGI(TAG, "sent publish returned msg_id=%d", msg_id);
    }
}

static void pub_task(void *arg)
{
    mqtt_message_t mess;
    isPubTaskRunning = true;

    for (;;)
    {
        if (xQueueReceive(publishQueue, &mess, (TickType_t)10 * configTICK_RATE_HZ) == pdPASS)
        {
            uint32_t ran = esp_random() % 50;
            vTaskDelay(ran / portTICK_PERIOD_MS);
            mqtt_app_publish(mess.topic, mess.mess);
        }
        // else
        // {
        //     mqtt_app_publish("/topic/ping", "ping");
        // }
    }
}

static void sub_task(void *arg)
{
    mqtt_message_t mess;
    isSubTaskRunning = true;
    receiveQueue = xQueueCreate(5, sizeof(mqtt_message_t));

    for (;;)
    {
    loop:
        if (xQueueReceive(receiveQueue, &mess, portMAX_DELAY) == pdPASS)
        {
            struct mqtt_node_t *temp = list_cb;
            for (; temp != NULL; temp = temp->next)
            {
                if (strcmp(temp->node.key, mess.topic) == 0)
                {
                    temp->node.cb(mess);
                    goto loop;
                }
            }
        }
    }
}

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
        if (isPubTaskRunning == false)
            xTaskCreate(pub_task, "pub Task", 5000, NULL, 5, NULL);
        if (isSubTaskRunning == false)
            xTaskCreate(sub_task, "sub Task", 5000, NULL, 5, NULL);
        struct mqtt_node_t *temp = list_cb;
        for (; temp != NULL; temp = temp->next)
        {
            if (esp_mqtt_client_subscribe(s_client, temp->node.key, 0) < 0)
            {
                ESP_LOGI(TAG, "Subscribe fail");
                esp_mqtt_client_disconnect(s_client);
                return ESP_OK;
            }
        }
        // if (esp_mqtt_client_subscribe(s_client, TopicPublish, 0) < 0)
        // {
        //     esp_mqtt_client_disconnect(s_client);
        // }

        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG, "MQTT_EVENT_DATA");
        ESP_LOGI(TAG, "TOPIC=%.*s", event->topic_len, event->topic);
        ESP_LOGI(TAG, "DATA=%.*s", event->data_len, event->data);
        mqtt_message_t receive_mess;
        memset(&receive_mess, 0, sizeof(mqtt_message_t));
        memcpy(receive_mess.topic, event->topic, event->topic_len);
        memcpy(receive_mess.mess, event->data, event->data_len);
        receive_mess.length = event->data_len;
        xQueueSend(
            receiveQueue,
            (void *)&receive_mess,
            (TickType_t)0);
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        break;
    default:
        ESP_LOGI(TAG, "Other event id:%d", event->event_id);
        break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%" PRId32 "", base, event_id);
    mqtt_event_handler_cb(event_data);
}

void mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
        .broker.address.uri = "mqtt://phaphh:1@192.168.137.1:1883",
        .session.keepalive = 10};
    s_client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(s_client, ESP_EVENT_ANY_ID, mqtt_event_handler, s_client);
    esp_mqtt_client_start(s_client);
}

void list_cb_add(char *key, mqtt_mess_cb_t v)
{
    if (list_cb == NULL)
    {
        list_cb = malloc(sizeof(struct mqtt_node_t));
        memset(list_cb->node.key, 0, sizeof(char) * CONFIG_MAX_TOPIC_LENGTH);
        memcpy(list_cb->node.key, key, strlen(key));

        list_cb->node.cb = v;
        list_cb->next = NULL;
    }
    else
    {
        struct mqtt_node_t *temp = list_cb;
        for (; temp->next != NULL; temp = temp->next)
            ;
        temp->next = malloc(sizeof(struct mqtt_node_t));
        temp = temp->next;
        memset(temp->node.key, 0, sizeof(char) * CONFIG_MAX_TOPIC_LENGTH);
        memcpy(temp->node.key, key, strlen(key));
        temp->node.cb = v;
        temp->next = NULL;
    }
}
void list_cb_print()
{
    struct mqtt_node_t *temp = list_cb;
    for (; temp != NULL; temp = temp->next)
    {
        ESP_LOGI("List_CB", "%s", temp->node.key);
    }
}