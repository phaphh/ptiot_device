#pragma once
#include "define.h"

typedef struct
{
    uint32_t p0;
    uint32_t p1;
    uint32_t size;
    uint32_t a;
    uint32_t step;
} group_parameter_t;

typedef struct
{
    uint32_t type;
    uint32_t e;
    uint32_t id;
    uint32_t p;
    uint32_t m;
    uint32_t v;
    uint32_t A;
    uint32_t T_h;
    uint32_t T_l;
} node_parameter_t;

struct iot_data_t
{
    uint32_t id;
    uint32_t *v;
};

struct iot_node_t
{
    struct iot_data_t d;
    struct iot_node_t *next;
};


void PTIOT_Init();
int update_Group(group_parameter_t v);
int update_Node(node_parameter_t v);
uint32_t update_Value(uint32_t v);
void add_node_from_message(char *mess);
uint8_t control(char *mess, size_t length);
void free_node_list();
extern uint32_t ptiot_value;