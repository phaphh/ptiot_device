#include "PTIOT.h"
#include "Storage.h"
#include "Network.h"
#include "esp_event.h"
#include "Mqtt_Task.h"
#include "math.h"
#include "ds18b20.h"

group_parameter_t g_data;
node_parameter_t n_data;

bool isRunning = false;
uint32_t *key = NULL;
uint32_t _pow(uint32_t a, uint32_t b, uint32_t p);
uint32_t _sqr(uint32_t x, uint32_t p);
uint32_t p2 = 0;
uint32_t ptiot_value = 0;
uint32_t last_value = 0;
char _data_String[MAX_BUFFER_MQTT_SIZE];
struct iot_node_t *child_node_list = NULL;
uint8_t SendFlag = false;

void add_node_data(uint32_t id, uint32_t *data);
void add_node_from_message(char *mess);
uint32_t *PTIOT_GetData();
void free_node_list();
int PTIOT_Update();
int key_generate();
void PTIOT_Publish();

bool Sensor_Update()
{
    mqtt_message_t mes;
    if (n_data.type == 1 || n_data.type == 2)
    {
        ds18b20_requestTemperatures();
        float cTemp = ds18b20_get_temp();
        if (cTemp > 100)
            return false;
        ptiot_value = (uint32_t)cTemp;
        if (ptiot_value == last_value)
            return false;
        last_value = ptiot_value;
        memset(&mes, 0, sizeof(mqtt_message_t));
        sprintf(mes.topic, "/updateValue");
        sprintf(mes.mess, MACSTR "/%lu", MAC2STR(STAMac), ptiot_value);
        if (isPubTaskRunning == true)
            xQueueSend(
                publishQueue,
                (void *)&mes,
                (TickType_t)0);
        return true;
    }
    return false;
}

uint8_t isIotTaskRunning = false;
static void ptiot_task(void *arg)
{
    isIotTaskRunning = true;
    vTaskDelay(15 * configTICK_RATE_HZ);
    for (;;)
    {
        if (isRunning == true && Sensor_Update() == true && isPubTaskRunning == true)
            PTIOT_Publish();
        SendFlag = false;
        vTaskDelay(2 * configTICK_RATE_HZ);
    }
}

void PTIOT_Publish()
{
    mqtt_message_t mes;
    if (PTIOT_Update() > 0)
    {
        memset(&mes, 0, sizeof(mqtt_message_t));

        if (esp_mesh_is_root() == true)
            snprintf(mes.topic, CONFIG_MAX_TOPIC_LENGTH, "/data/root");
        else
            snprintf(mes.topic, CONFIG_MAX_TOPIC_LENGTH, "/data/" MACSTR, MAC2STR(PMAC));

        memcpy(mes.mess, _data_String, MAX_BUFFER_MQTT_SIZE);
        if (isPubTaskRunning == true)
        {
            xQueueSend(
                publishQueue,
                (void *)&mes,
                (TickType_t)0);
            SendFlag = true;
        }
    }
}

void PTIOT_Init()
{
    if (isIotTaskRunning == false)
        xTaskCreate(ptiot_task, "pub Task", 5000, NULL, 4, NULL);
    isRunning = false;
    esp_err_t err = NVS_GetBlob("group", &g_data, sizeof(group_parameter_t));
    ESP_LOGI("Group", "%lu,%lu,%lu,%lu,%lu", g_data.a, g_data.p0, g_data.p1, g_data.size, g_data.step);
    p2 = g_data.p0 * g_data.p1;
    if (err != ESP_OK)
        return;
    err = NVS_GetBlob("node", &n_data, sizeof(node_parameter_t));
    if (err != ESP_OK)
        return;
    ESP_LOGI("Node", "%lu,%lu", n_data.type, n_data.p);
    if (n_data.type == 1 || n_data.type == 2)
        if (key_generate() <= 0)
            return;

    ESP_LOGI("Node", "isRunning");
    isRunning = true;
}

int key_generate()
{
    if (g_data.size <= 0 || n_data.e < 2 || n_data.p < 2 || n_data.id >= g_data.size || g_data.p1 < 2 || g_data.p0 < 2)
        return -1;
    if (key != NULL)
        free(key);
    key = malloc(sizeof(uint32_t) * g_data.size);
    memset(key, 0, sizeof(uint32_t) * g_data.size);
    *(key + n_data.id) = n_data.e;

    for (uint32_t i = n_data.id + 1, temp = n_data.e; i < g_data.size; i++)
        *(key + i) = temp = _pow(temp, n_data.p, p2);
    return 1;
}

int update_Group(group_parameter_t v)
{
    isRunning = false;
    if (v.p0 < 2 || v.p1 < 2 || v.size < 2)
        return -1;
    if (v.p0 * v.p1 >= 0xFFFFFF)
        return -2;
    esp_err_t err = NVS_SetBlob("group", (void *)(&v), sizeof(group_parameter_t));
    if (err != ESP_OK)
        return -1;
    err = NVS_GetBlob("group", &g_data, sizeof(group_parameter_t));
    if (g_data.p0 != v.p0 || g_data.p1 != v.p1 || g_data.size != v.size)
        return -1;
    PTIOT_Init();
    return 0;
}

int update_Node(node_parameter_t v)
{
    isRunning = false;
    if (v.p < 2 || v.type > 5)
        return -1;
    esp_err_t err = NVS_SetBlob("node", (void *)(&v), sizeof(node_parameter_t));
    if (err != ESP_OK)
        return -1;
    err = NVS_GetBlob("node", &n_data, sizeof(node_parameter_t));
    if (err != ESP_OK)
        return -1;
    PTIOT_Init();
    return 0;
}

int PTIOT_Update()
{
    struct iot_node_t *temp = child_node_list;
    if (isRunning == false)
        return -1;
    uint32_t *C = NULL;
    if (n_data.type == 1 || n_data.type == 2)
    {
        C = PTIOT_GetData();
    }
    else
    {
        C = (uint32_t *)malloc(sizeof(uint32_t) * g_data.size);
        memset(C, 0, sizeof(uint32_t) * g_data.size);
    }

    for (; temp != NULL; temp = temp->next)
    {
        for (int i = 0; i < g_data.size; i++)
        {
            *(C + i) = (*(C + i) + *(temp->d.v + i)) % p2;
        }
    }

    memset(_data_String, 0, MAX_BUFFER_MQTT_SIZE);
    snprintf(_data_String, MAX_BUFFER_MQTT_SIZE, "%ld/", n_data.id);
    for (int i = 0; i < g_data.size; i++)
        sprintf(_data_String + strlen(_data_String), "%08lx", *(C + i));
    free(C);
    return 1;
}

uint32_t GaussDistributed()
{
    float res = ONE_DIV_SQRT_TWO_PI * (float)n_data.A / (float)n_data.v;
    float temp = -pow(ptiot_value - n_data.m, 2);
    temp = temp / ((float)(2 * pow(n_data.v, 2)));
    temp = pow(EULER_NUMBER, temp);
    res = res * temp;
    return (uint32_t)round(res);
}

uint32_t *PTIOT_GetData()
{
    uint32_t *temp = malloc(sizeof(uint32_t) * g_data.size);
    memset(temp, 0, sizeof(uint32_t) * g_data.size);
    uint32_t _value = 0;
    if (n_data.type == 1)
        _value = GaussDistributed();
    else if (n_data.type == 2)
        _value = ptiot_value;
    else
        return temp;

    for (int i = 0; i < g_data.size; i++)
        *(temp + i) = (_value * key[i]) % p2;

    return temp;
}

void free_node_list()
{
    struct iot_node_t *temp = child_node_list;
    for (; temp != NULL;)
    {
        struct iot_node_t *temp1 = temp;
        temp = temp->next;
        if (temp1->d.v != NULL)
        {
            free(temp1->d.v);
        }

        free(temp1);
        temp1 = NULL;
    }
    child_node_list = NULL;
}

void add_node_from_message(char *mess)
{
    char str[50];
    memset(str, 0, 50);
    uint32_t id;
    ESP_LOGI("add_node_from_message", "isRunning");
    if (isRunning == false)
        return;
    ESP_LOGI("add_node_from_message", "isRunning Pass");

    int ret = sscanf(mess, "%lu/%s", &id, str);
    if (ret != 2)
        return;
    ESP_LOGI("add_node_from_message", "1");
    uint32_t *data = malloc(sizeof(uint32_t) * g_data.size);
    for (int i = 0; i < g_data.size - 1; i++)
        if (sscanf(str, "%8lx%s", (data + i), str) != 2)
            return;
    ESP_LOGI("add_node_from_message", "2");
    if (sscanf(str, "%8lx", (data + g_data.size - 1)) != 1)
        return;
    ESP_LOGI("add_node_from_message", "id:%ld,mess:%s", id, mess);
    add_node_data(id, data);
    Sensor_Update();
    PTIOT_Publish();
}

void add_node_data(uint32_t id, uint32_t *data)
{
    if (child_node_list == NULL)
    {
        child_node_list = malloc(sizeof(struct iot_node_t));
        child_node_list->d.id = id;
        child_node_list->d.v = data;
        child_node_list->next = NULL;
    }
    else
    {
        ESP_LOGI("add_node_data", "1");
        struct iot_node_t *temp = child_node_list;
        for (; temp != NULL; temp = temp->next)
        {
            if (temp->d.id == id)
            {
                if (temp->d.v != NULL)
                    free(temp->d.v);
                temp->d.v = data;
                return;
            }
            if (temp->next == NULL)
            {
                struct iot_node_t *newNode = malloc(sizeof(struct iot_node_t));
                newNode->d.id = id;
                newNode->d.v = data;
                newNode->next = NULL;
                temp->next = newNode;
                return;
            }
        }
    }
}

float decode_Controller(char *mess, size_t length)
{
    float res = 0;
    if (length % 16 == 0)
        return FP_NAN;
    uint32_t temp = 0, temp1 = 0;
    int ret = sscanf(mess, "%8lx%s", &temp, mess);
    ESP_LOGI("Decode 1", "%d,%ld,%s,%d", ret, temp, mess, length);
    if (ret == 2)
    {
        // size_t len = (length - 8);
        res = temp;
        for (int i = 0; i < length - 16; i = i + 16)
        {

            ret = sscanf(mess, "%8lx%8lx%s", &temp, &temp1, mess);
            temp = temp % g_data.p1;
            temp1 = temp1 % g_data.p1;
            if (ret == 3 || ret == 2)
                res += (float)(temp / (float)temp1);
            else
                return FP_NAN;
        }
    }
    else if (ret == 1)
    {
        ESP_LOGI("Decode", "%ld,%s", temp, mess);
        res = temp % g_data.p1;
    }

    return res;
}

uint8_t control(char *mess, size_t length)
{
    float res = decode_Controller(mess, length);
    ESP_LOGI("Control", "%ld,%f,%ld,%ld", n_data.type, res, n_data.T_l, n_data.T_h);
    if (n_data.type != 3 || length % 16 == 0)
        return 3;
    if (res < n_data.T_l || res > n_data.T_h)
    {
        return 1;
    }
    return 0;
}

uint32_t update_Value(uint32_t v)
{
    ptiot_value = v;
    return ptiot_value;
}

uint32_t _sqr(uint32_t x, uint32_t p)
{
    return (x * x) % p;
}

uint32_t _pow(uint32_t a, uint32_t b, uint32_t p)
{
    if (b == 0)
        return 1;
    else if (b % 2 == 0)
        return _sqr(_pow(a, b / 2, p), p);
    else
        return (a * (_sqr(_pow(a, b / 2, p), p))) % p;
}
