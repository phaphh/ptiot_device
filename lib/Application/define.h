#pragma once
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_mac.h"
#define MAX_BUFFER_MQTT_SIZE 256

#define CONFIG_MESH_CHANNEL 0
#define CONFIG_MESH_ROUTER_SSID "ptiot"
#define CONFIG_MESH_ROUTER_PASSWD "12341234"
#define CONFIG_WIFI_AUTH_WPA2_PSK 1
#define CONFIG_MESH_AP_AUTHMODE 3
#define CONFIG_MESH_AP_PASSWD "MAP_PASSWD"
#define CONFIG_MESH_AP_CONNECTIONS 2
#define CONFIG_MESH_NON_MESH_AP_CONNECTIONS 0
#define CONFIG_MESH_MAX_LAYER 6
#define CONFIG_MESH_ROUTE_TABLE_SIZE 50
#define CONFIG_MAX_TOPIC_LENGTH 50
#define CONFIG_MAX_NAME_LENGTH 20

#define ONE_DIV_SQRT_TWO_PI 0.39894228040143267793994605993438
#define EULER_NUMBER 2.7182818284590452353602874713527

#define SQRT_TWO_PI 2.506628274631000502415765284811
#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559
#define DEG_TO_RAD 0.017453292519943295769236907684886
#define RAD_TO_DEG 57.295779513082320876798154814105

typedef struct
{
    char topic[CONFIG_MAX_TOPIC_LENGTH];
    char mess[MAX_BUFFER_MQTT_SIZE];
    size_t length;
} mqtt_message_t;

typedef void (*mqtt_mess_cb_t)(mqtt_message_t);

struct mqtt_node_data_t
{
    char key[CONFIG_MAX_TOPIC_LENGTH];
    mqtt_mess_cb_t cb;
};

struct mqtt_node_t
{
    struct mqtt_node_data_t node;
    struct mqtt_node_t *next;
};