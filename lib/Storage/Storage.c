#include "Storage.h"

uint32_t _handle;

esp_err_t NVS_Init(){
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    return nvs_open(STORAGE_NAMESPACE,  NVS_READWRITE, &_handle);
}

esp_err_t NVS_SetI32(char * k, int32_t v){
    esp_err_t err = nvs_set_i32(_handle, k, v);
    if(err){
        return err;
    }
    return nvs_commit(_handle);
}

esp_err_t NVS_GetI32(char *k,int32_t *out_value){
    return nvs_get_i32(_handle, k, out_value);
}

esp_err_t NVS_SetU32(char * k, uint32_t v){
    esp_err_t err = nvs_set_u32(_handle, k, v);
    if(err){
        return err;
    }
    return nvs_commit(_handle);
}

esp_err_t NVS_GetU32(char *k,uint32_t *out_value){
    return nvs_get_u32(_handle, k, out_value);
}

esp_err_t NVS_SetString(char * k, char * v){
    esp_err_t err = nvs_set_str(_handle, k, v);
        if(err){
            return err;
        }
        return nvs_commit(_handle);
}

esp_err_t NVS_GetString(char *k,char *out_value,size_t *length){
    return nvs_get_str(_handle, k, out_value,length);
}

esp_err_t NVS_SetBlob(char * k, void * v,size_t size){
    esp_err_t err = nvs_set_blob(_handle, k, v,size);
        if(err){
            return err;
        }
        return nvs_commit(_handle);
}
esp_err_t NVS_GetBlob(char *k,void *v, size_t size){
    return nvs_get_blob(_handle, k, v,&size);
}