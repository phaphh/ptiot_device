#pragma once
#include "define.h"
#include "nvs_flash.h"
#include "nvs.h"
#define STORAGE_NAMESPACE "nvs"

esp_err_t NVS_Init();

esp_err_t NVS_SetI32(char * k, int32_t v);
esp_err_t NVS_GetI32(char *k,int32_t *out_value);
esp_err_t NVS_SetU32(char * k, uint32_t v);
esp_err_t NVS_GetU32(char *k,uint32_t *out_value);
esp_err_t NVS_SetString(char * k, char * v);
esp_err_t NVS_GetString(char *k,char *out_value,size_t *length);

esp_err_t NVS_SetBlob(char * k, void * v,size_t size);
esp_err_t NVS_GetBlob(char *k,void *v, size_t size);

