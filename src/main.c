#include "Network.h"
#include "Storage.h"
#include "esp_log.h"
#include "PTIOT.h"
#include "board.h"
#include "Mqtt_Task.h"
#include "ds18b20.h"

void ping_toptic(mqtt_message_t mess)
{
    ESP_LOGI("Ping", "%s", mess.mess);
    if (strcmp("pingAll", mess.mess) != 0)
        return;
    char name[CONFIG_MAX_NAME_LENGTH];
    size_t length = CONFIG_MAX_NAME_LENGTH;
    char parent[CONFIG_MAX_NAME_LENGTH];
    memset(parent, 0, sizeof(char) * CONFIG_MAX_NAME_LENGTH);
    memset(name, 0, sizeof(char) * CONFIG_MAX_NAME_LENGTH);
    NVS_GetString("name", name, &length);
    mqtt_message_t mes;
    memset(&mes, 0, sizeof(mqtt_message_t));
    sprintf(mes.topic, "/ping/response");
    if (esp_mesh_is_root() == true)
        sprintf(parent, "root");
    else
        sprintf(parent, MACSTR, MAC2STR(mesh_parent_addr.addr));

    sprintf(mes.mess, MACSTR "/%s/%s/%ld", MAC2STR(STAMac), name, parent,ptiot_value);
    if (isPubTaskRunning == true)
    {
        xQueueSend(
            publishQueue,
            (void *)&mes,
            (TickType_t)(0));
    }
}

void name_setting(mqtt_message_t mess)
{
    char name[CONFIG_MAX_NAME_LENGTH];
    memset(name, 0, CONFIG_MAX_NAME_LENGTH);
    int res = sscanf(mess.mess, "%s", name);
    if (res == 1 && strlen(mess.mess) < CONFIG_MAX_NAME_LENGTH - 1)
        NVS_SetString("name", name);
}

void node_setting(mqtt_message_t mess)
{
    // ESP_LOGI("Setting", "%s", mess.mess);
    node_parameter_t temp;
    int res = 0;
    if (mess.mess[0] == '1')
    {
        res = sscanf(mess.mess, "%lu,%lu,%lu,%lu,%lu,%lu,%lu", &temp.type, &temp.e, &temp.id, &temp.p, &temp.m, &temp.v, &temp.A);
        if (res == 7)
            res = 1;
    }
    else if (mess.mess[0] == '2')
    {
        res = sscanf(mess.mess, "%lu,%lu,%lu,%lu,%lu", &temp.type, &temp.e, &temp.id, &temp.p, &temp.A);
        if (res == 5)
            res = 1;
    }
    else if (mess.mess[0] == '3')
    {
        res = sscanf(mess.mess, "%lu,%lu,%lu,%lu,%lu", &temp.type, &temp.id, &temp.p, &temp.T_l, &temp.T_h);
        if (res == 5)
            res = 1;
    }

    if (res == 1)
        update_Node(temp);
}

void node_restart(mqtt_message_t mess)
{
    if (strcmp("clear", mess.mess) != 0)
        return;
    nvs_flash_erase();
    nvs_flash_init();
    PTIOT_Init();
}

void group_setting(mqtt_message_t mess)
{
    group_parameter_t temp;
    int res = sscanf(mess.mess, "%lu,%lu,%lu", &temp.p0, &temp.p1, &temp.size);
    if (res == 3)
        update_Group(temp);
}

void update_C(mqtt_message_t mess)
{
    add_node_from_message(mess.mess);
}

void update_Data(mqtt_message_t mess)
{
    uint32_t temp;
    int res = sscanf(mess.mess, "%lu", &temp);
    if (res == 1)
        update_Value(temp);
}

void receive_Controller(mqtt_message_t mess)
{
    uint8_t status = control(mess.mess, mess.length);
    switch (status)
    {
    case 0:
    case 1:
        status = GPIO_Set(status);
        break;
    default:
        return;
    }
    mqtt_message_t mes;
    memset(&mes, 0, sizeof(mqtt_message_t));
    sprintf(mes.topic, "/status");
    sprintf(mes.mess, MACSTR "/%hhu", MAC2STR(STAMac), status);
    if (isPubTaskRunning == true)
        xQueueSend(
            publishQueue,
            (void *)&mes,
            (TickType_t)0);
}

void controler_Led(mqtt_message_t mess)
{
    uint8_t status;
    int res = sscanf(mess.mess, "%hhu", &status);
    if (res != 1)
        return;

    switch (status)
    {
    case 0:
    case 1:
        status = GPIO_Set(status);
        break;
    case 2:
        status = GPIO_Toggle();
        break;
    default:
        return;
    }
    mqtt_message_t mes;
    memset(&mes, 0, sizeof(mqtt_message_t));
    sprintf(mes.topic, "/status");
    sprintf(mes.mess, MACSTR "/%hhu", MAC2STR(STAMac), status);
    if (isPubTaskRunning == true)
        xQueueSend(
            publishQueue,
            (void *)&mes,
            (TickType_t)0);
}

void button_handler()
{
    uint8_t res = GPIO_Toggle();
    mqtt_message_t mes;
    memset(&mes, 0, sizeof(mqtt_message_t));
    sprintf(mes.topic, "/status");
    sprintf(mes.mess, MACSTR "/%hhu", MAC2STR(STAMac), res);
    if (isPubTaskRunning == true)
        xQueueSend(
            publishQueue,
            (void *)&mes,
            (TickType_t)0);
}

void app_main()
{
    char str[50];
    NVS_Init();
    NetworkInit();
    PTIOT_Init();
    GPIO_Init(button_handler);
    TempSensor_Init();
    // list_cb_add("/ping",ping_toptic);
    memset(str, 0, 50);
    sprintf(str, "/group/setting/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, group_setting);
    memset(str, 0, 50);
    sprintf(str, "/node/setting/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, node_setting);
    memset(str, 0, 50);
    sprintf(str, "/node/setting/clear");
    list_cb_add(str, node_restart);
    memset(str, 0, 50);
    sprintf(str, "/data/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, update_C);
    memset(str, 0, 50);
    sprintf(str, "/control/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, controler_Led);

    memset(str, 0, 50);
    sprintf(str, "/node/name/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, name_setting);
    memset(str, 0, 50);
    sprintf(str, "/ping");
    list_cb_add(str, ping_toptic);

    memset(str, 0, 50);
    sprintf(str, "/node/data/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, update_Data);

    memset(str, 0, 50);
    sprintf(str, "/node/control/" MACSTR, MAC2STR(STAMac));
    list_cb_add(str, receive_Controller);

    list_cb_print();
}
